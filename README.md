### What is in the repository ###

* Android app source code
* Web app source code
* Database table

### Database setup ###

* Get the database structure file in the database directory inside the web and test directories
* Create a database and upload the file.
* Change the database connection depending on the database name 

### Android Setup ###

* Change the url according to your IP address and web app location
* Run the app on an emulator or mobile
